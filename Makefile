all:
	 g++ -std=c++17 `pkg-config --cflags opencv4` cv.cpp `pkg-config --libs opencv4` -o extractor
clean:
	rm -f extractor
test:
	./extractor -input test.png -output result.png
