/**

     photo-to-dimensioned-image
     Copyright (C) 2023  frezia Tadeusz Puźniakowski

     This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

     This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

     You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.


*/

#include <json.hpp>
#include <webapi.hpp>
#include <iostream>
#include <opencv2/aruco.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <set>
#include <vector>
#include <signal.h>
#include <random>

#include "tp_args.hpp"

// g++ `pkg-config --cflags opencv4` cv.cpp `pkg-config --libs opencv4`

using namespace std;
using namespace cv;

class MarkerConfig {
public:
    const double DPI_CONVERSION = 0.03937008;

    double DPI; /// ppmm =  0.03937008 * 72dpi

    double WIDTH;  // [mm]
    double HEIGHT; // [mm]
    cv::Point2f MARKERS_FRAME_DIM() {
        return {(float) (WIDTH * (DPI * DPI_CONVERSION)), (float) (HEIGHT * (DPI * DPI_CONVERSION))};
    } // square 10x10

    std::vector<cv::Point2f> MARKER_POINTS() {
        return {cv::Point2f{0, 0}, cv::Point2f{MARKERS_FRAME_DIM().x, 0},
                cv::Point2f{MARKERS_FRAME_DIM().x, MARKERS_FRAME_DIM().y},
                cv::Point2f{0, MARKERS_FRAME_DIM().y}};
    }

    MarkerConfig(double width = 200, double height = 200, double dpi = 72) {
        DPI = dpi; /// ppmm =  0.03937008 * 72dpi

        WIDTH = width;   // [mm]
        HEIGHT = height; // [mm]
    }
};

std::vector<cv::Point2f> find_my_markers(cv::Mat &src_image) {
    const static std::set<int> MY_MARKERS = {20, 21, 22, 23};

    std::vector<int> markerIds;
    std::vector<std::vector<cv::Point2f>> markerCorners, rejectedCandidates;
    cv::Ptr<cv::aruco::DetectorParameters> parameters =
            cv::aruco::DetectorParameters::create();
    cv::Ptr<cv::aruco::Dictionary> dictionary =
            cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
    cv::aruco::detectMarkers(src_image, dictionary, markerCorners, markerIds,
                             parameters, rejectedCandidates);

    std::vector<std::vector<cv::Point2f>> my_markers(4);

    std::vector<std::vector<cv::Point2f>> ret(4);
    for (int i = 0; i < markerIds.size(); i++) {
        if (MY_MARKERS.count(markerIds[i])) {
            ret[markerIds[i] - 20] = markerCorners[i];
        }
    }
    for (auto p: ret)
        if (p.size() == 0)
            return {};
    std::vector<cv::Point2f> ret_;
    int idx = 0;
    for (auto e: ret)
        ret_.push_back(e[(idx++) % 4]);
    return ret_;
}

cv::Mat fix_perspective_to_dimensions(cv::Mat _image,
                                      vector<cv::Point2f> marker_coords,
                                      MarkerConfig &markerConfig) {
    const auto MARKER_POINTS = markerConfig.MARKER_POINTS();
    const auto MARKERS_FRAME_DIM = markerConfig.MARKERS_FRAME_DIM();
    cv::Mat _undistortedImage;
    cv::Matx33f M = cv::getPerspectiveTransform(marker_coords, MARKER_POINTS);
    cv::warpPerspective(
            _image, _undistortedImage, M,
            cv::Size(MARKERS_FRAME_DIM.x,
                     MARKERS_FRAME_DIM.y)); // cv::Size(_image.cols, _image.rows));
//    imshow("ppp", _undistortedImage);
    return _undistortedImage;
}

cv::Mat remove_noise_and_other_artifacts(cv::Mat _image, int margin_size = 20) {

    cv::cvtColor(_image, _image, cv::COLOR_BGR2GRAY);


    auto orig = _image.clone();

    auto kernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, {5, 5});
    auto kernel2 = cv::getStructuringElement(cv::MORPH_ELLIPSE, {margin_size, margin_size});
    auto kernel_for_erosion = cv::getStructuringElement(cv::MORPH_ELLIPSE, {2, 2});

    //morphologyEx(orig, orig, cv::MORPH_CLOSE, kernel);
    morphologyEx(orig, orig, cv::MORPH_OPEN, kernel);
    cv::erode(orig, orig, kernel_for_erosion);

    morphologyEx(_image, _image, cv::MORPH_CLOSE, kernel);


    cv::erode(_image, _image, kernel2);

    cv::bitwise_or(_image, orig, _image);

    return _image;
}


cv::Mat make_it_back_and_white(cv::Mat _image, int thr = 170) {
    cv::threshold(_image, _image, thr, 255, cv::THRESH_BINARY);
    return _image;
}

cv::Mat get_only_the_middle_contour(cv::Mat _image) {
    std::vector<std::vector<cv::Point> > contours;
    //std::vector<int> hierarchy;
    cv::imwrite("__debug.png", _image);
//            cv::findContours( _image, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE );
    cv::findContours(_image, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
    std::vector<std::vector<cv::Point> > contours_ready;
    //  C++: double pointPolygonTest(InputArray contour, Point2f pt, bool measureDist)
    for (auto c: contours) {
        if (cv::pointPolygonTest(c, cv::Point2f(_image.cols / 2, _image.rows / 2), true) >= 0) {
            contours_ready.push_back(c);
            break;
        }
    }

    auto debug = _image.clone();
    cv::Mat contourImage(_image.size(), CV_8UC3, cv::Scalar(0, 0, 0));
    for (size_t idx = 0; idx < contours_ready.size(); idx++) {
        cv::drawContours(contourImage, contours_ready, idx, cv::Scalar(255, 255, 255),
                         cv::FILLED);
    }
    // cv::imshow("contours", contourImage);
    cvtColor(contourImage,contourImage, COLOR_BGR2GRAY);
    auto kernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, {8, 8});
    morphologyEx(_image, _image, cv::MORPH_CLOSE, kernel);
    cv::bitwise_and(contourImage,_image, contourImage);


    cv::bitwise_not(contourImage, contourImage);
    auto [x, y, w, h] = cv::boundingRect(contours_ready[0]);
    Mat cropped_image = contourImage(Range(y - 3, y + h + 6), Range(x - 3, x + w + 6));
    return cropped_image;//contourImage;
}


cv::Mat process_image(cv::Mat src_image, double frame_width, double frame_height, double dpi, int margin_size, int threshold = 170) {
    auto whole_marker = find_my_markers(src_image);

    if (whole_marker.size() == 4) {
        MarkerConfig markerConfig(frame_width, frame_height, dpi);

        cv::Mat found_image = src_image.clone();

        found_image = remove_noise_and_other_artifacts(found_image, margin_size);
        found_image =
                fix_perspective_to_dimensions(found_image, whole_marker, markerConfig);
        found_image = make_it_back_and_white(found_image, threshold);
        found_image = get_only_the_middle_contour(found_image);
        // imshow("found_image", found_image);
        return found_image;
    } else {
        throw std::invalid_argument("could not find markers on the image");
    }

}

bool is_thread_working = true;

void on_ctrl_c_signal(int) {
    is_thread_working = 0;  // terminate motors thread
}


int main(int argc, char **argv) {
    using namespace tp::args;

    auto server = arg(argc, argv, "server", false, "start http sever?");
    auto input_filename = arg(argc, argv, "input", (std::string) "../test3.png", "input filename");
    auto output_filename =
            arg(argc, argv, "output", (std::string) "result.png", "output filename");
    auto do_markers =
            arg(argc, argv, "do_markers", false, "should you do markers");

    auto frame_width =
            arg(argc, argv, "frame_width", 1000.0, "the width of the frame in [mm]");
    auto frame_height =
            arg(argc, argv, "frame_height", 1500.0, "the width of the frame in [mm]");
    auto dpi =
            arg(argc, argv, "dpi", 72.0, "the output image dpi");
    // margin_size
    auto margin_size =
            arg(argc, argv, "margin_size", 20, "the size of edge marks (nacinki)");
    auto threshold =
            arg(argc, argv, "threshold", 170, "the black/white threshold between 1 and 255");

    auto show_help = arg(argc, argv, "help", false);
    if (show_help) {
        args_info(std::cout);
        return 0;
    }
    // todo: get dimensions

    if (do_markers) {
        for (int i = 0; i < 128; i++) {
            cv::Mat markerImage;
            cv::Ptr<cv::aruco::Dictionary> dictionary =
                    cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
            cv::aruco::drawMarker(dictionary, i, 200, markerImage, 1);
            cv::imwrite(("marker_" + to_string(i) + ".png").c_str(), markerImage);
        }
        std::cout << "markers created. Put then in the following order: "
                  << std::endl;
        std::cout << "   20                                  21         "
                  << std::endl;
        std::cout << "                                                  "
                  << std::endl;
        std::cout << "                                                  "
                  << std::endl;
        std::cout << "                                                  "
                  << std::endl;
        std::cout << "                                                  "
                  << std::endl;
        std::cout << "   23                                  22         "
                  << std::endl;
        return 0;
    } else {
        if (server) {
            using namespace pigcd;
            using namespace std::chrono_literals;
            signal(SIGINT, on_ctrl_c_signal);
            signal(SIGTERM, on_ctrl_c_signal);
            webapi::mini_http_c srv(webapi::server_host, "3016");
            srv.on_request(
                    "GET", "/apidoc",
                    [&srv](pigcd::webapi::request_data_t &, pigcd::webapi::response_data_t &response,
                           const std::vector<std::string> &) {
                        response.send(srv.generate_apidoc());
                    },
                    "WebAPI Documentation", "Generates the documentation page.");
            srv.on_request(
                    "POST", "/api/convert",
                    [&srv, &frame_height, &frame_width, &dpi,&margin_size, &threshold](pigcd::webapi::request_data_t &request,
                                                              pigcd::webapi::response_data_t &response,
                                                              const std::vector<std::string> &) {
                        auto [method, url, headers, body, user_data] = request;

                        auto body_string = std::string(body.begin(), body.end());
                        // std::cout << "converting: " << body_string << std::endl;

                        nlohmann::json body_json = nlohmann::json::parse(body_string);

                        auto data = pigcd::webapi::base64tobin(body_json["image"]);
                        if (body_json.contains("frame_width")) {
                            frame_width = body_json["frame_width"];
                        }
                        if (body_json.contains("frame_height")) {
                            frame_height = body_json["frame_height"];
                        }
                        if (body_json.contains("margin_size")) {
                            margin_size = body_json["margin_size"];
                        }
                        if (body_json.contains("dpi")) {
                            dpi = body_json["dpi"];
                        }
                        if (body_json.contains("threshold")) {
                            threshold = body_json["threshold"];
                        }

                        response.headers.push_back({"Content-Type", "application/json"});
                        std::cout << "dpi: " << dpi << "  [" << frame_width << ", " << frame_height << "]\n";
                        Mat src_image = cv::imdecode(data, cv::IMREAD_COLOR);//, int flags);
                        try {
                            static int id = 0;
                            auto found_image = process_image(src_image, frame_width, frame_height, dpi, margin_size, threshold);
                            std::string output_filename = "__result_" + std::to_string(id++) +".png";
                            imwrite(pigcd::webapi::static_files_directory + "/" + output_filename, found_image);
                            nlohmann::json result = {{"result_url",output_filename}};
                            response.send(result.dump());
                        } catch (std::exception &err){
                            nlohmann::json result = {{"error",err.what()}};
                            response.send(result.dump());
                        }

                    },
                    "Convert image to one shape", "This will find one shape in the middle and scale it accordingly");
            srv.on_request("GET", ".*", pigcd::webapi::handle_static, "Static contents",
                           "Returns the simple web interface");
            srv.server([]() { return !(is_thread_working == 0); });
        } else {
            unsigned long int key;
            Mat src_image = imread(input_filename);
            auto found_image = process_image(src_image, frame_width, frame_height, dpi,margin_size, threshold);
            imwrite(output_filename, found_image);
            std::cout << "saved image: " << output_filename << std::endl;
            // key = waitKey(0);
        }
    }
    return 0;
}
